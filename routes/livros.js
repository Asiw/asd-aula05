var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
require('../models/livro');
var Livro = mongoose.model('livros');


router.get('/', findAllLivros);
router.post('/', addLivro);
router.get('/:id', findLivroById);
router.put('/:id', updateLivro);
router.delete('/:id', deleteLivro);



function findAllLivros(req, res) {
	Livro.find(function(err, livros) {
		if(err)
			res.json({'ERROR': err});
		else
			res.json(livros);
	});
};

function addLivro(req, res) {
	var newLivro = new Livro({
		nome: req.body.nome,
		editora: req.body.editora,
		ano: req.body.ano,
		isbn: req.body.isbn,
		_autores: req.body._autores,
		criticas: req.body.criticas
	});

	newLivro.save(function(err){
		if(err)
			res.json({'ERROR': err});
		else
			res.json({'SUCCESS': newLivro});
	});
};

function findLivroById(req, res){
	Livro.findById(req.params.id, function(err, livro){
		if(err)
			res.json({'ERROR': err});
		else
			res.json(livro)
	});
};

function updateLivro(req, res) {
	Livro.findById(req.params.id, function(err, livro){
		livro.nome = req.body.nome;
		livro.editora = req.body.editora;
		livro.ano = req.body.ano;
		livro.isbn = req.body.isbn;
		livro._autores = req.body._autores;
		livro.criticas = req.body.criticas;
		
		livro.save(function(err){
			if(err)
				res.json({'ERROR': err});
			else
				res.json({'UPDATED': livro});
		});
	});
};

function deleteLivro(req, res) {
	Livro.findById(req.params.id, function(err, livro){
		if(err)
			res.json({'ERROR': err});
		else {
			livro.remove(function(err){
				if(err)
					res.json({'ERROR': err});
				else
					res.json({'REMOVED': livro});
			});
		};
	});
};



module.exports = router;
