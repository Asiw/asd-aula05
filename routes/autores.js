var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
require('../models/autor');
var Autor = mongoose.model('autores');


router.get('/', findAllAutores);
router.get('/query/:querystring', queryAutores);
router.post('/', addAutor);
router.get('/:id', findAutorById);
router.put('/:id', updateAutor);
router.delete('/:id', deleteAutor);



function findAllAutores(req, res) {
	Autor.find(function(err, autores) {
		if(err)
			res.json({'ERROR': err});
		else
			res.json(autores);
	});
};

function queryAutores(req, res) {
	console.log(req.params.querystring);
	var query = Autor.findOne({nome: new RegExp(req.params.querystring, 'i')});
	query.select('nome');
	query.exec(function(err, autores){
		if(err) return res.json({'ERROR': err});
		console.log(autores);
		return res.json(autores);
	});
};

function addAutor(req, res) {
	var newAutor = new Autor({
		nome: req.body.nome,
	});

	newAutor.save(function(err){
		if(err)
			res.json({'ERROR': err});
		else
			res.json({'SUCCESS': newAutor});
	});
};

function findAutorById(req, res){
	Autor.findById(req.params.id, function(err, autor){
		if(err)
			res.json({'ERROR': err});
		else
			res.json(autor)
	});
};

function updateAutor(req, res) {
	Autor.findById(req.params.id, function(err, autor){
		autor.nome = req.body.nome;


		autor.save(function(err){
			if(err)
				res.json({'ERROR': err});
			else
				res.json({'UPDATED': autor});
		});
	});
};

function deleteAutor(req, res) {
	Autor.findById(req.params.id, function(err, autor){
		if(err)
			res.json({'ERROR': err});
		else {
			autor.remove(function(err){
				if(err)
					res.json({'ERROR': err});
				else
					res.json({'REMOVED': autor});
			});
		};
	});
};



module.exports = router;
