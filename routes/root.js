var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	res.render('index', {'project_name': 'Portal'});
});

module.exports = router;
