process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var mongoose = require('mongoose');
var server = require('../app');

var Aluno = require('../models/aluno');

var should = chai.should();

chai.use(chaiHttp);


describe('Alunos', function() {

	this.timeout(0);

	Aluno.collection.drop();

	beforeEach(function(done){
		var newAluno = new Aluno({
			nome: 'Foo bar',
			cpf: '11111111111'
		});
		newAluno.save(function(err){
			done();
		});
	});
	afterEach(function(done){
		Aluno.collection.drop();
		done();
	});


	it('should add a SINGLE aluno on /alunos POST', function(done) {
		chai.request(server)
			.post('/alunos')
			.send({'nome': 'Rafael Riedel', 'cpf': '04242174659'})
			.end(function(err, res){
				res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.a('object'),
				res.body.should.have.property('SUCCESS');
				res.body.SUCCESS.should.be.a('object');
				res.body.SUCCESS.should.have.property('nome');
				res.body.SUCCESS.should.have.property('cpf');
				res.body.SUCCESS.should.have.property('_id');
				res.body.SUCCESS.nome.should.equal('Rafael Riedel');
				res.body.SUCCESS.cpf.should.equal('04242174659');
				done();
			});
	});


	it('should list ALL alunos on /alunos GET', function(done){
  		chai.request(server)
  			.get('/alunos')
  			.end(function(err, res) {
  				console.log(res.body);
  				res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.a('array');
				res.body[0].should.have.property('_id');
				res.body[0].should.have.property('nome');
				res.body[0].should.have.property('cpf');
				res.body[0].nome.should.equal('Foo bar');
				res.body[0].cpf.should.equal('11111111111');
  				done();
  			});
  	});

	it('should list a SINGLE aluno on /aluno/<id> GET', function(done){
		var newAluno = new Aluno({
			nome: 'SpamEggBacon',
			cpf: '12345678901'
		});
		newAluno.save(function(err, data){
			chai.request(server)
				.get('/aluno/' + data.id)
				.end(function(err, res){
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('_id');
					res.body.should.have.property('nome');
					res.body.should.have.property('cpf');
					res.body.nome.should.equal('SpamEggBacon');
					res.body.cpf.should.equal('12345678901');
					res.body._id.should.equal(data.id);
					done();
				});
		});
	});
	
	it('should update a SINGLE aluno on /aluno/<id> PUT', function(done){
		chai.request(server)
			.get('/alunos')
			.end(function(err, res){
				console.log(res.body);
				chai.request(server)
				.put('/aluno/'+res.body[0]._id)
				.send({'nome': 'Spider'})
				.end(function(error, response){
					response.should.have.status(200);
					response.should.be.json;
					response.body.should.be.a('object');
					response.body.should.have.property('UPDATED');
					response.body.UPDATED.should.be.a('object');
					response.body.UPDATED.should.have.property('nome');
					response.body.UPDATED.should.have.property('_id');
					response.body.UPDATED.nome.should.equal('Spider');
					done();
				});
			});
	});

	it('should delete a SINGLE aluno on /aluno/<id> DELETE', function(done){
		chai.request(server)
			.get('/alunos')
			.end(function(err, res){
				console.log(res.body);
				chai.request(server)
				.delete('/aluno/'+res.body[0]._id)
				.end(function(error, response){
					console.log(response.body);
					response.should.have.status(200);
					response.should.be.json;
					response.body.should.be.a('object');
					response.body.should.have.property('REMOVED');
					response.body.REMOVED.should.be.a('object');
					response.body.REMOVED.should.have.property('nome');
					response.body.REMOVED.should.have.property('_id');
					response.body.REMOVED.nome.should.equal('Foo bar');
					done();
				});
			});
	});
});