FROM debian:latest

MAINTAINER Rafael Riedel <rafael.riedel@gmail.com>

RUN apt-get update && apt-get install -y \ 
	curl
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

RUN apt-get install -y \
	nodejs \
	build-essential \
	git

RUN mkdir -p /src/app
WORKDIR /src/app

COPY package.json /src/app/
RUN npm install

COPY . /src/app

EXPOSE 3000

CMD ["npm", "start"]