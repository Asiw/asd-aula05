var mongoose = require('mongoose');

var livroSchema = new mongoose.Schema({
	isbn: String,
	nome: String,
	ano: Number,
	_autores: [{nome: String}],
	editora: String,
	criticas: [{critica: String, data: Date}]
});
	

module.exports = mongoose.model('livros', livroSchema);