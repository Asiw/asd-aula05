var app = angular.module('poc', ['ui.router', 'ngMaterial', 'ngMdIcons', 'ngResource']);

app.config(function($stateProvider, $urlRouterProvider, $mdIconProvider){
	// quando nao encontrar uma URL, redireciona para o /state1
	//$urlRouterProvider.otherwise("/state1");

	// configurando os estados

	$stateProvider
		.state('livros', {
			url: '/livros',
			templateUrl: 'partials/livros.html',
			resolve: {
				livrosPromisse: ['$stateParams', 'Livro', function($stateParams, Livro){
					return Livro.query();
				}]
			},
			controller: function($scope, $timeout, $mdSidenav, $log, $state, livrosPromisse) {
				var originatorEv;

				$scope.livros = livrosPromisse;				

				$scope.toggleLeft = buildDelayedToggler('left');
			
				this.openMenu = function($mdOpenMenu, ev) {
			      originatorEv = ev;
			      $mdOpenMenu(ev);
			    };

			    function buildDelayedToggler(navID) {
			      return debounce(function() {
			        // Component lookup should always be available since we are not using `ng-if`
			        $mdSidenav(navID)
			          .toggle()
			          .then(function () {
			            $log.debug("toggle " + navID + " is done");
			          });
			      }, 200);
			    }

			    function debounce(func, wait, context) {
			      var timer;

			      return function debounced() {
			        var context = $scope,
			            args = Array.prototype.slice.call(arguments);
			        $timeout.cancel(timer);
			        timer = $timeout(function() {
			          timer = undefined;
			          func.apply(context, args);
			        }, wait || 10);
			      };
			    }
			}
			
		})

		.state('viewLivro',{
			url: '/livros/:id/view',
			templateUrl: 'partials/livro-view.html',
			controller: 'LivrosController'
		})

		.state('newLivro', {
			url: '/livros/new',
			templateUrl: 'partials/livro-add.html',
			controller: function($scope, Livro) {
				$scope.livro = new Livro();

				$scope.addLivro = function() {
					$scope.livro.$save(function(){
						console.log("livro salvo");
						$state.go('livros');
					});		
				};
			}
		})

		.state('editLivro',{
			url: '/livros/:id',
			templateUrl: 'partials/livro-edit.html',
			resolve: {
				livroPromisse: ['$stateParams', 'Livro', 
				function($stateParams, Livro) {
					return Livro.get({id: $stateParams.id })
				}]
			},
			controller: function($scope, $stateParams, $mdDialog, livroPromisse, Autor, Critica) {
				$scope.livro = livroPromisse;

			    $scope.newAutor = function(autor) {
			    	a = new Autor({'nome': autor});			    	
			    	$scope.livro._autores.push(a);
			    	return  a;
			    }

			    $scope.showPrompt = function(e) {
			    	var confirm = $mdDialog.prompt()
			    		.title('Crítica para o livro \'' + $scope.livro.nome + '\'')
			    		.placeholder('Critica')
			    		.ariaLabel('Critica')
			    		.targetEvent(e)
			    		.ok('Adicionar')
			    		.cancel('Cancelar');

			    	$mdDialog.show(confirm).then(function(result){
			    		var critica = new Critica({critica: result, data: new Date()});
			    		$scope.livro.criticas.push(critica);
			    	});
			    };



				$scope.updateLivro = function() {
					$scope.livro.$update(function(){
						$state.go('livros');
					});
				};
			}
		});
})

.factory('Livro', ['$resource', function($resource) {
	return $resource('/api/livros/:id', {id: '@_id' }, {
		update: {
			method: 'PUT'
		}
	});

}])

.factory('Autor', ['$resource', function($resource){
	return $resource('/api/autores/:id', {id: '@_id'},{
		update: {
			method: 'PUT'
		}
	})
}])

.factory('Critica', ['$resource', function($resource){
	return $resource('/api/criticas/:id', {id: '@_id'},{
		update: {
			method: 'PUT'
		}
	})
}])


.run(function($state){
	$state.go('livros');
});



app.controller('LivrosController', ['$scope', '$timeout', '$mdSidenav', '$log', '$state', 'Livro',//'cursos', 'portal', 'auth',	
	function($scope, $timeout, $mdSidenav, $log, $state, Livro) {
		var o = {};

		o.getLivro = function() {
			return $scope.livro = Livro.get({id: $stateParams.id});
		}

		o.listaLivros = function() {
			return $scope.livros = Livro.query();
		}


	}
]);